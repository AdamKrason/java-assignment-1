package heroes;

import equipment.Armor;
import equipment.ArmorType;
import equipment.Weapon;
import equipment.WeaponType;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import java.text.DecimalFormat;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    Hero mage;
    Armor armor1, armor2;
    Weapon weapon, weapon2;

    protected DecimalFormat df = new DecimalFormat("0.00");


    @BeforeEach
    void initializeHero(){
        mage = new Mage("Adam");
    }

    @Test
    void createHero_MageName_ShouldCreateMageWithCorrectName() {
        String expected = "Adam";
        String actual = mage.getName();
        assertEquals(expected, actual);
    }

    @Test
    void createHero_MageLevel_ShouldCreateMageWithCorrectLevel() {
        mage = new Mage("Adam");
        int expected = 1;
        int actual = mage.getLevel();
        assertEquals(expected, actual);
    }

    @Test
    void createHero_MageAttributes_ShouldCreateMageWithCorrectAttributes() {
        mage = new Mage("Adam");
        HeroAttributes expected = new HeroAttributes(1,1,8);
        HeroAttributes actual = mage.getAttributes();
        assertEquals(expected, actual);
    }

    @Test
    void createHero_WarriorAttributes_ShouldCreateWarriorWithCorrectAttributes() {
        Warrior warrior = new Warrior("Timmy");
        HeroAttributes expected = new HeroAttributes(5,2,1);
        HeroAttributes actual = warrior.getAttributes();
        assertEquals(expected, actual);
    }
    @Test
    void createHero_RogueAttributes_ShouldCreateRogueWithCorrectAttributes() {
        Rogue rogue = new Rogue("Timmy");
        HeroAttributes expected = new HeroAttributes(2,6,1);
        HeroAttributes actual = rogue.getAttributes();
        assertEquals(expected, actual);
    }

    @Test
    void createHero_RangerAttributes_ShouldCreateRangerWithCorrectAttributes() {
        Ranger ranger = new Ranger("Timmy");
        HeroAttributes expected = new HeroAttributes(1,7,1);
        HeroAttributes actual = ranger.getAttributes();
        assertEquals(expected, actual);
    }

    @Test
    void levelUpHero_HeroLevel_ShouldSetHeroLevelCorrectlyAfterLevelUp() {
        mage = new Mage("Adam");
        mage.levelUp();
        int expected = 2;
        int actual = mage.getLevel();
        assertEquals(expected, actual);
    }
    @Test
    void levelUpMage_MageAttributes_ShouldSetMageAttributesCorrectlyAfterLevelUp() {
        mage = new Mage("Adam");
        mage.levelUp();
        HeroAttributes expected = new HeroAttributes(2,2,13);
        HeroAttributes actual = mage.getAttributes();
        assertEquals(expected, actual);
    }

    @Test
    void levelUpRanger_RangerAttributes_ShouldSetRangerAttributesCorrectlyAfterLevelUp() {
        mage = new Ranger("Adam");
        mage.levelUp();
        HeroAttributes expected = new HeroAttributes(2,12,2);
        HeroAttributes actual = mage.getAttributes();
        assertEquals(expected, actual);
    }

    @Test
    void levelUpRogue_RogueAttributes_ShouldSetRogueAttributesCorrectlyAfterLevelUp() {
        mage = new Rogue("Adam");
        mage.levelUp();
        HeroAttributes expected = new HeroAttributes(3,10,2);
        HeroAttributes actual = mage.getAttributes();
        assertEquals(expected, actual);
    }

    @Test
    void levelUpWarrior_WarriorAttributes_ShouldSetWarriorAttributesCorrectlyAfterLevelUp() {
        mage = new Warrior("Adam");
        mage.levelUp();
        HeroAttributes expected = new HeroAttributes(8,4,2);
        HeroAttributes actual = mage.getAttributes();
        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_EquippingArmor_ShouldEquipArmorIntoRightEquipmentSlot() {
        armor1 = new Armor("Chestplate", ArmorType.CLOTH,
                Slot.BODY, 1, 2,1,0);
        try {
            mage.equipArmor(armor1);
        } catch (InvalidArmorException e) {
            System.out.println(e.getMessage());;
        }

        Armor expected = new Armor("Chestplate", ArmorType.CLOTH,
                Slot.BODY, 1, 2,1,0);
        Armor actual = mage.getBodyArmor();
        assertEquals(actual, expected);
    }

    @Test
    void equipArmor_EquippingInvalidArmorLevel_ShouldThrowInvalidArmorException() {
        armor1 = new Armor("Chestplate", ArmorType.PLATE,
                Slot.BODY, 1, 2,1,0);
        Exception exception =  assertThrows(InvalidArmorException.class,
                () -> mage.equipArmor(armor1));
        String actual = exception.getMessage();
        String expected = "This hero can't equip this armor type";
    }

    @Test
    void equipArmor_EquippingInvalidArmorType_ShouldThrowInvalidArmorException() {
        armor1 = new Armor("Chestplate", ArmorType.CLOTH,
                Slot.BODY, 2, 2,1,0);
        Exception exception =  assertThrows(InvalidArmorException.class,
                () -> mage.equipArmor(armor1));
        String actual = exception.getMessage();
        String expected = "This hero is too low level for this armor";
    }

    @Test
    void equipWeapon_EquippingWeapon_ShouldEquipWeaponCorrectly() {
        weapon = new Weapon("Staff", WeaponType.STAFF,
                1, 1);
        try {
            mage.equipWeapon(weapon);
        } catch (InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }
        Weapon expected = new Weapon("Staff", WeaponType.STAFF,
                1, 1);
        Weapon actual = mage.getWeapon();
        assertEquals(expected, actual);
    }

    @Test
    void equipWeapon_EquippingInvalidWeaponLevel_ShouldThrowInvalidWeaponException() {
        weapon = new Weapon("Staff", WeaponType.STAFF,
                3, 1);
        Exception exception =  assertThrows(InvalidWeaponException.class,
                () -> mage.equipWeapon(weapon));
        String actual = exception.getMessage();
        String expected = "This hero is too low level for this weapon";
    }

    @Test
    void equipWeapon_EquippingInvalidWeaponType_ShouldThrowInvalidWeaponException() {
        weapon = new Weapon("Staff", WeaponType.SWORD,
                1, 1);
        Exception exception =  assertThrows(InvalidWeaponException.class,
                () -> mage.equipWeapon(weapon));
        String actual = exception.getMessage();
        String expected = "This hero can't equip this weapon type";
    }

    @Test
    void totalAttributes_noEquipment_ShouldShowCorrectTotalAttributesWithNoEquipment() {
        HeroAttributes actual = mage.totalAttributes();
        HeroAttributes expected = new HeroAttributes(1,1,8);
        assertEquals(expected, actual);
    }

    @Test
    void totalAttributes_OneArmor_ShouldShowCorrectTotalAttributesWithOneArmor() {
        armor1 = new Armor("cool rag", ArmorType.CLOTH,
                Slot.BODY, 1, 1,1,10);
        try {
            mage.equipArmor(armor1);
        } catch (InvalidArmorException e) {
            System.out.println(e.getMessage());
        }
        HeroAttributes actual = mage.totalAttributes();
        HeroAttributes expected = new HeroAttributes(2,2,18);
        assertEquals(expected, actual);
    }

    @Test
    void totalAttributes_TwoArmor_ShouldShowCorrectTotalAttributesWithTwoArmor() {
        armor1 = new Armor("cool rag", ArmorType.CLOTH,
                Slot.BODY, 1, 1,1,10);
        armor2 = new Armor("very cool hat", ArmorType.CLOTH,
                Slot.HEAD, 1, 1,2,6);
        try {
            mage.equipArmor(armor1);
            mage.equipArmor(armor2);
        } catch (InvalidArmorException e) {
            System.out.println(e.getMessage());
        }
        HeroAttributes actual = mage.totalAttributes();
        HeroAttributes expected = new HeroAttributes(3,4,24);
        assertEquals(expected, actual);
    }

    @Test
    void totalAttributes_ReplacedArmor_ShouldShowCorrectTotalAttributesWithReplaceArmor() {
        armor1 = new Armor("cool rag", ArmorType.CLOTH,
                Slot.BODY, 1, 1,1,10);
        armor2 = new Armor("not so cool rag", ArmorType.CLOTH,
                Slot.BODY, 1, 1,1,4);
        try {
            mage.equipArmor(armor1);
            mage.equipArmor(armor2);
        } catch (InvalidArmorException e) {
            System.out.println(e.getMessage());
        }
        HeroAttributes actual = mage.totalAttributes();
        HeroAttributes expected = new HeroAttributes(2,2,12);
        assertEquals(expected, actual);
    }

    @Test
    void HeroDamage_NoWeapon_ShouldShowCorrectHeroDamageWithNoWeaponEquipped() {
        double expected = 1 * (1+8/100.0);
        double actual = mage.damage();
        assertEquals(expected, actual);
    }

    @Test
    void HeroDamage_WithWeapon_ShouldShowCorrectHeroDamageWithWeaponEquipped() {
        weapon = new Weapon("Staff",
                WeaponType.STAFF, 1, 4);
        try {
            mage.equipWeapon(weapon);
        } catch (InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }
        double expected = 4 * (1+8/100.0);
        double actual = mage.damage();
        assertEquals(expected, actual);
    }

    @Test
    void HeroDamage_WithreplacedWeapon_ShouldShowCorrectHeroDamageWithReplacedWeaponEquipped() {
        weapon = new Weapon("Staff",
                WeaponType.STAFF, 1, 4);
        weapon2 = new Weapon("Broken Staff",
                WeaponType.STAFF, 1, 7);

        try {
            mage.equipWeapon(weapon);
            mage.equipWeapon(weapon2);
        } catch (InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }
        double expected = Double.parseDouble(df.format(7 * (1+8/100.0)));
        double actual = mage.damage();
        assertEquals(expected, actual);
    }

    @Test
    void HeroDamage_WithWeaponAndArmor_ShouldShowCorrectHeroDamageWithWeaponAndArmorEquipped() {
        weapon = new Weapon("Staff",
                WeaponType.STAFF, 1, 4);
        armor1 = new Armor("cool rag", ArmorType.CLOTH,
                Slot.BODY, 1, 1, 1, 10);
        try {
            mage.equipWeapon(weapon);
            mage.equipArmor(armor1);
        } catch (InvalidWeaponException | InvalidArmorException e) {
            System.out.println(e.getMessage());
        }
        double expected = 4 * (1+18/100.0);
        double actual = mage.damage();
        assertEquals(expected, actual);
    }
}