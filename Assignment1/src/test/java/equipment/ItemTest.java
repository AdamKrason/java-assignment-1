package equipment;

import heroes.HeroAttributes;
import heroes.Slot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {
    Armor armor;
    Weapon weapon;
    @BeforeEach
    void initializeItems(){
        armor = new Armor("Chestplate", ArmorType.PLATE,
                Slot.BODY, 2, 2,1,0);
        weapon = new Weapon("Staff", WeaponType.STAFF,
                1, 1);
    }
    @Test
    void createArmor_ArmorName_ShouldCreateArmorWithCorrectName() {
        String expected = "Chestplate";
        String actual = armor.getName();
        assertEquals(expected, actual);
    }
    @Test
    void createArmor_ArmorRequiredLevel_ShouldCreateArmorWithCorrectRequiredLevel() {
        int expected = 2;
        int actual = armor.getRequiredLevel();
        assertEquals(expected, actual);
    }

    @Test
    void createArmor_ArmorSlot_ShouldCreateArmorWithCorrectSlot() {
        Slot expected = Slot.BODY;
        Slot actual = armor.getSlot();
        assertEquals(expected, actual);
    }

    @Test
    void createArmor_ArmorType_ShouldCreateArmorOfCorrectType() {
        ArmorType expected = ArmorType.PLATE;
        ArmorType actual = armor.getArmorType();
        assertEquals(actual, expected);
    }

    @Test
    void createArmor_ArmorAttributes_ShouldCreateArmorWithCorrectAttributes() {
        HeroAttributes expected = new HeroAttributes(2,1,0);
        HeroAttributes actual = armor.getItemAttributes();
        assertEquals(actual, expected);
    }

    @Test
    void createWeapon_WeaponName_ShouldCreateWeaponWithCorrectName() {
        String expected = "Staff";
        String actual = weapon.getName();
        assertEquals(expected, actual);
    }

    @Test
    void createWeapon_WeaponRequiredLevelShouldCreateWeaponWithCorrectRequiredLevel() {
        int expected = 1;
        int actual = weapon.getRequiredLevel();
        assertEquals(expected, actual);
    }

    @Test
    void createWeapon_WeaponType_ShouldCreateWeaponOfCorrectType() {
        WeaponType expected = WeaponType.STAFF;
        WeaponType actual = weapon.getWeaponType();
        assertEquals(actual, expected);
    }

    @Test
    void createWeapon_WeaponDamage_ShouldCreateWeaponWithCorrectDamageStat() {
        int expected = 1;
        int actual = weapon.getDamage();
        assertEquals(expected, actual);
    }
}