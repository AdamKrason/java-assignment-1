package heroes;

import equipment.ArmorType;
import equipment.WeaponType;

/**
 * The Warrior class is a subclass of the Hero class.
 */
public class Warrior extends Hero{

    /**
     * Warrior constructor takes in  only name, but it also sets the hero's
     * attributes, valid weapons and valid armor types.
     * @param name Name of the hero
     */
    public Warrior(String name) {
        super(name);
        this.attributes = new HeroAttributes(5,2,1);
        this.validArmor.add(ArmorType.PLATE);
        this.validArmor.add(ArmorType.MAIL);
        this.validWeapons.add(WeaponType.SWORD);
        this.validWeapons.add(WeaponType.AXE);
        this.validWeapons.add(WeaponType.HAMMER);
    }
    @Override
    public void levelUp(){
        this.level ++;
        this.attributes.updateAttributes(3,2,1);
    }

    @Override
    public double damage() {
        if (!this.equipment.containsKey(Slot.WEAPON)){
            return 1+this.totalAttributes().getStrength()/100.0;
        }
        return Double.parseDouble(df.format(this.getWeapon().getDamage() * (1.0+this.totalAttributes().getStrength()/100.0)));
    }
}