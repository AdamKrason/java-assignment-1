package heroes;

import equipment.*;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;

import java.text.DecimalFormat;
import java.util.*;

/**
 * The hero class holds the variables all heroes have in common, which are
 * the name, level, hero attributes, equipment, the weapons they can use,
 * the armor they can use.
 */
public abstract class Hero {

    protected DecimalFormat df = new DecimalFormat("0.00");

    protected String name;
    protected int level = 1;
    protected HeroAttributes attributes;

    protected HashMap<Slot, Item> equipment = new HashMap<>();
    protected Set<WeaponType> validWeapons = new HashSet<>();
    protected Set<ArmorType> validArmor = new HashSet<>();

    /**
     * The constructor takes in one parameter which is the name.
     * @param name The name of the hero.
     */
    public Hero(String name) {
        this.name = name;
    }

    /**
     * Creates a string that displays the stats of the hero and
     * the equipment the hero currently holds.
     * @return String displaying the hero.
     */
    public String displayHero(){
        StringBuilder str = new StringBuilder();
        str.append("Name: ");
        str.append(this.name);
        str.append("- Level: ");
        str.append(level);
        str.append(" - Class: ");
        str.append(this.getClass().getName().replace("heroes.", ""));
        str.append("\nStats: ");
        str.append("Strength: ");
        str.append(this.attributes.getStrength());
        str.append(" - Dexterity: ");
        str.append(this.attributes.getDexterity());
        str.append(" - Intelligence: ");
        str.append(this.attributes.getIntelligence());
        if (equipment.containsKey(Slot.WEAPON)){
            str.append("\nCurrent weapon equipped: ");
            str.append(equipment.get(Slot.WEAPON));
        }
        if (equipment.containsKey(Slot.BODY)){
            str.append("\n Current body armor equipped: ");
            str.append(equipment.get(Slot.BODY));
        }
        if (equipment.containsKey(Slot.HEAD)){
            str.append("\nCurrent head armor equipped: ");
            str.append(equipment.get(Slot.HEAD));
        }
        if (equipment.containsKey(Slot.LEGS)){
            str.append("\nCurrent leg armor equipped: ");
            str.append(equipment.get(Slot.LEGS));
        }

        str.append("\nHero damage: ");
        str.append(this.damage());
        return str.toString();
    }


    /**
     * Takes the heroes attributes and adds all the attributes
     * gained from equipped armor into a total sum of attributes.
     * @return returns a new HeroAttributes object with the added attributes.
     */
    public HeroAttributes totalAttributes(){
        HeroAttributes temp;
        int strength, dexterity, intelligence;
        strength = this.attributes.getStrength();
        dexterity = this.attributes.getDexterity();
        intelligence = this.attributes.getIntelligence();

        for (Map.Entry<Slot, Item> val : equipment.entrySet()){
            if (val.getKey().equals(Slot.WEAPON)){
                continue;
            }
            temp = val.getValue().getItemAttributes();
            strength += temp.getStrength();
            dexterity += temp.getDexterity();
            intelligence += temp.getIntelligence();
        }

        return new HeroAttributes(strength, dexterity, intelligence);
    }

    /**
     * Calculates the damage the hero will do using a formula that
     * uses the hero's damaging attribute and the damage of the weapon
     * if the hero has one.
     * @return Returns the damage as a double.
     */
    public abstract double damage();

    /**
     * Levels the hero up one level and modifies
     * the hero attributes accordingly to the hero type.
     */
    public abstract void levelUp();

    /**
     * Equips a weapon in the weapon slot. If the slot already has a
     * weapon it just puts the new weapon in the slot instead.
     * @param weapon The weapon being equipped.
     * @throws InvalidWeaponException Throws exception if the hero tries to equip invalid weapon.
     */
    public void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (!this.validWeapons.contains(weapon.getWeaponType())){
            throw new InvalidWeaponException("This hero can't equip this weapon type");
        }
        if ((this.level < weapon.getRequiredLevel())){
            throw new InvalidWeaponException("This hero is too low level for this weapon");
        }
        equipment.put(Slot.WEAPON, weapon);
    };

    /**
     * Equips armor into the slot the armor uses. If there is armor in that slot
     * already the new armor is put in the slot instead.
     * @param armor the armor being equipped.
     * @throws InvalidArmorException Throws exception if the hero tries to equip invalid armor.
     */
    public void equipArmor(Armor armor) throws InvalidArmorException {
        if (!this.validArmor.contains(armor.getArmorType())){
            throw new InvalidArmorException("This hero can't equip this armor type");
        }
        if ((this.level < armor.getRequiredLevel())){
            throw new InvalidArmorException("This hero is too low level for this armor");
        }
        equipment.put(armor.getArmorSlot(), armor);
    }

    public String getName() {
        return this.name;
    }

    public int getLevel() {
        return level;
    }

    public HeroAttributes getAttributes() {
        return attributes;
    }

    public Armor getBodyArmor() {
        return (Armor) this.equipment.get(Slot.BODY);
    }

    public Armor getLegArmor(){
        return (Armor) this.equipment.get(Slot.LEGS);
    }

    public Armor getHeadArmor(){
        return (Armor) this.equipment.get(Slot.HEAD);
    }

    public Weapon getWeapon() {
        return (Weapon) equipment.get(Slot.WEAPON);
    }
}

