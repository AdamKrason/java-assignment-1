package heroes;

import equipment.ArmorType;
import equipment.WeaponType;

/**
 * The Mage class is a subclass of the Hero class.
 */
public class Mage extends Hero{

    /**
     * Mage constructor takes in  only name, but it also sets the hero's
     * attributes, valid weapons and valid armor types.
     * @param name Name of the hero
     */
    public Mage(String name) {
        super(name);
        this.attributes = new HeroAttributes(1,1,8);
        this.validArmor.add(ArmorType.CLOTH);
        this.validWeapons.add(WeaponType.STAFF);
        this.validWeapons.add(WeaponType.WAND);
    }

    @Override
    public void levelUp(){
        this.level ++;
        this.attributes.updateAttributes(1,1,5);
    }

    @Override
    public double damage() {
        if (!this.equipment.containsKey(Slot.WEAPON)){
            return 1+this.totalAttributes().getIntelligence()/100.0;
        }
        return Double.parseDouble(df.format(this.getWeapon().getDamage() * (1.0+this.totalAttributes().getIntelligence()/100.0)));
    }
}
