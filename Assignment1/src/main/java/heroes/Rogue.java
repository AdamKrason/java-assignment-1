package heroes;

import equipment.ArmorType;
import equipment.WeaponType;

/**
 * The Rogue class is a subclass of the Hero class.
 */
public class Rogue extends Hero{

    /**
     * Rogue constructor takes in  only name, but it also sets the hero's
     * attributes, valid weapons and valid armor types.
     * @param name Name of the hero
     */
    public Rogue(String name) {
        super(name);
        this.attributes = new HeroAttributes(2,6,1);
        this.validArmor.add(ArmorType.LEATHER);
        this.validArmor.add(ArmorType.MAIL);
        this.validWeapons.add(WeaponType.DAGGER);
        this.validWeapons.add(WeaponType.SWORD);
    }

    @Override
    public void levelUp(){
        this.level ++;
        this.attributes.updateAttributes(1,4,1);
    }

    @Override
    public double damage() {
        if (!this.equipment.containsKey(Slot.WEAPON)){
            return 1+this.totalAttributes().getDexterity()/100.0;
        }
        return Double.parseDouble(df.format(this.getWeapon().getDamage() * (1.0+this.totalAttributes().getDexterity()/100.0)));
    }
}
