package heroes;

import equipment.ArmorType;
import equipment.WeaponType;

/**
 * The Ranger class is a subclass of the hero class.
 */
public class Ranger extends Hero{

    /**
     * Ranger constructor takes in  only name, but it also sets the hero's
     * attributes, valid weapons and valid armor types.
     * @param name Name of the hero
     */
    public Ranger(String name) {
        super(name);
        this.attributes = new HeroAttributes(1,7,1);
        this.validArmor.add(ArmorType.LEATHER);
        this.validArmor.add(ArmorType.MAIL);
        this.validWeapons.add(WeaponType.BOW);
    }
    @Override
    public void levelUp(){
        this.level ++;
        this.attributes.updateAttributes(1,5,1);
    }

    @Override
    public double damage() {
        if (!this.equipment.containsKey(Slot.WEAPON)){
            return 1+this.totalAttributes().getDexterity()/100.0;
        }
        return Double.parseDouble(df.format(this.getWeapon().getDamage() * (1.0+this.totalAttributes().getDexterity()/100.0)));
    }
}
