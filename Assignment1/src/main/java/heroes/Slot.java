package heroes;

/**
 * The Slot enumerator consists of four slot in which
 * items can be equipped.
 */
public enum Slot {
    WEAPON,
    HEAD,
    BODY,
    LEGS
}
