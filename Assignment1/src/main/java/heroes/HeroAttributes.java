package heroes;

import java.util.Objects;

/**
 * The HeroAttributes class hold the strength, dexterity and intelligence
 * variables. These are used to display the stats of a hero and the stats
 * armor gives to heroes.
 */
public class HeroAttributes {
    private int strength, dexterity, intelligence;

    /**
     * Creates a HeroAttribute object to store the three stat types.
     * @param strength The strength of the hero or that the armor gives.
     * @param dexterity The dexterity of the hero or that the armor gives.
     * @param intelligence The intelligence of the hero or that the armor gives.
     */
    public HeroAttributes(int strength, int dexterity, int intelligence){
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    /**
     * Updates the three stats by adding the current stats and the stats
     * sent in as arguments in the method.
     * @param strength Updates the strength stat.
     * @param dexterity Updates the dexterity stat.
     * @param intelligence Updates the intelligence stat.
     */
    public void updateAttributes(int strength, int dexterity, int intelligence) {
        this.strength += strength;
        this.dexterity += dexterity;
        this.intelligence += intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HeroAttributes that = (HeroAttributes) o;
        return strength == that.strength && dexterity == that.dexterity && intelligence == that.intelligence;
    }

    @Override
    public int hashCode() {
        return Objects.hash(strength, dexterity, intelligence);
    }

    public int getIntelligence() {
        return intelligence;
    }
}
