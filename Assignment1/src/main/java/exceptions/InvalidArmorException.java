package exceptions;

/**
 * The InvalidArmorException is thrown when a hero tries to
 * equip armor that has a too high required level or is incompatible with
 * the types of armor the hero can wear.
 */
public class InvalidArmorException extends Exception{
    public InvalidArmorException(String message){
        super(message);
    }
}
