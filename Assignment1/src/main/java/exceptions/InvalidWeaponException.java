package exceptions;

/**
 * The InvalidWeaponException is thrown when a hero tries to equip
 * a weapon that has a too high required level for the hero or
 * can't be used be the hero because of the weapon type.
 */
public class InvalidWeaponException extends Exception{
    public InvalidWeaponException(String message){
        super(message);
    }
}
