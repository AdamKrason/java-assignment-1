package equipment;

/**
 * The ArmorType enumerator gives us four types of armor we can create.
 */
public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
