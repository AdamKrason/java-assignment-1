package equipment;

/**
 * The WeaponType enumerator gives us seven types of weapons to create.
 */
public enum WeaponType {
    AXE,
    BOW,
    WAND,
    STAFF,
    DAGGER,
    HAMMER,
    SWORD
}
