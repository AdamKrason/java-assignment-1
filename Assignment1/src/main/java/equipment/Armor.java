package equipment;

import equipment.Item;
import heroes.Hero;
import heroes.HeroAttributes;
import heroes.Slot;

import java.util.Objects;

/**
 * Armor is a subclass of Item. The class also hold the
 * variables armorType and itemAttributes.
 */
public class Armor extends Item {
    ArmorType armorType;
    HeroAttributes itemAttributes;

    /**
     * The constructor creates the armor object.
     * @param name Name of the armor.
     * @param type The type of the armor (ArmorType).
     * @param slot The slot the armor equips into.
     * @param reqLevel Required level of the hero to equip this armor
     * @param strength The strength stat the armor gives.
     * @param dexterity The dexterity stat the armor gives.
     * @param intelligence The intelligence stat the armor gives-
     */
    public Armor(String name, ArmorType type, heroes.Slot slot, int reqLevel, int strength, int dexterity, int intelligence){
        super(name, reqLevel, slot);
        this.itemAttributes = new HeroAttributes(strength,dexterity,intelligence);
        this.armorType = type;
    }

    public ArmorType getArmorType() {
        return this.armorType;
    }

    public HeroAttributes getItemAttributes(){
        return itemAttributes;
    }

    public Slot getArmorSlot() {
        return this.slot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Armor armor = (Armor) o;
        return armorType == armor.armorType && Objects.equals(itemAttributes, armor.itemAttributes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(armorType, itemAttributes);
    }
}
