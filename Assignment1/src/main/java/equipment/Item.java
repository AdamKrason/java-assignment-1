package equipment;

import heroes.HeroAttributes;
import heroes.Slot;

/**
 * The item class is the abstract class for both the weapon
 * and armor classes. This item keeps track of the items name,
 * required level and the slot it uses.
 */
public abstract class Item {

    protected String name;
    protected int reqLevel;
    Slot slot;

    /**
     * The constructor creates an item object with different stats.
     * @param name The name of the item.
     * @param reqLevel The requiredLevel of the hero to equip this item.
     */
    public Item(String name, int reqLevel, Slot slot){
        this.name = name;
        this.reqLevel = reqLevel;
        this.slot = slot;
    }

    @Override
    public java.lang.String toString() {
        return this.name;
    }

    public int getRequiredLevel(){
        return this.reqLevel;
    }

    public String getName() { return this.name; }

    public Slot getSlot() { return this.slot; }

    public abstract HeroAttributes getItemAttributes();
}
