package equipment;

import equipment.Item;
import heroes.Hero;
import heroes.HeroAttributes;
import heroes.Slot;

import java.util.Objects;

/**
 * The Weapon class is a subclass of Item. In addition to the variables in
 * Item it holds weaponType and weaponDamage.
 */

public class Weapon extends Item {
    WeaponType weaponType;
    int weaponDamage;

    /**
     * The constructor creates the Weapon object.
     * @param name The name of the Weapon.
     * @param type The weapon type (WeaponType)
     * @param reqLevel The required level of the hero to equip this weapon.
     * @param weaponDamage The weapons damage.
     */
    public Weapon(String name, WeaponType type, int reqLevel, int weaponDamage){
        super(name, reqLevel, heroes.Slot.WEAPON);
        this.weaponType = type;
        this.weaponDamage = weaponDamage;
    }

    public WeaponType getWeaponType(){
        return this.weaponType;
    }

    public int getDamage() { return this.weaponDamage; }

    @Override
    public HeroAttributes getItemAttributes() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Weapon weapon = (Weapon) o;
        return weaponDamage == weapon.weaponDamage && weaponType == weapon.weaponType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(weaponType, weaponDamage);
    }
}
