# Java Assignment 1


## Description

The program simulates a RPG game structure with different hero types that can equip different types of weapons and armor. Heroes have stats like name, level attributes and items they are able to equip. Items are split into two groups, where item boost the hero's damage and armor boost their total attributes. 

## Installation 
Install jdk 17 on your computer. 
Clone the repo into a folder using git clone "this repo name"

## Usage
The program has no functionality in main. The only thing to run
are the tests that make sure the methods meet the requirements of the assignment
and work correctly.

To run the test right click the test folder in intellij and click the button to run all
tests in the project. 